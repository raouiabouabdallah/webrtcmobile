
import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  TouchableOpacity,
  Dimensions,
  Button,
  TouchableWithoutFeedback,
  Animated
} from 'react-native';




import {
  RTCPeerConnection,
  RTCIceCandidate,
  RTCSessionDescription,
  RTCView,
  MediaStream,
  MediaStreamTrack,
  mediaDevices,
  registerGlobals
} from 'react-native-webrtc';

import io from 'socket.io-client'
import Icon from 'react-native-vector-icons/FontAwesome';


import Sound from 'react-native-sound';



const dimensions = Dimensions.get('window')



class App extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      localStream: null,
      remoteStream: null,
      sp: "Empty",
      showBouton: true,
      startCall: false,
      toggleMute: false,
      switchCamera: false,
      closeOpenCamera:false,
      anser:false,
      goto:1
    }

    this.sp=null
    this.socket = null
    this.candidates = []
    this.outgoingCall = null;
    this.incomingCall = null;
    this.endCall = null;
    // this.pc = new RTCPeerConnection(pc_config)
    this.moveAnimation = new Animated.ValueXY({ x: 10, y: 290 })

  }



  componentWillUnmount =()=>{
    if (this.pc) {
      this.pc.removeStream(this.state.localStream)
      this.pc.removeStream(this.state.remoteStream)
      this.pc.close();
    }
    this.pc.close();
    this.sp=null;
    this.setState({localStream:null, remoteStream:null})
    this.pc=null,

this.setState({
  localStream: null,
  remoteStream: null,
  sp: "Empty",
  showBouton: true,
  startCall: false,
  toggleMute: false,
  switchCamera: false,
  closeOpenCamera:false,
  anser:false

})
    this.socket = null
    this.candidates = []
    this.outgoingCall = null;
    this.incomingCall = null;
    this.endCall = null;

  }




  playSound = type => {
    switch (type) {
      case 'outgoing':
        this.outgoingCall = new Sound('dialing.mp3', null, (e) => {
          if (e) {
              // console.log('error', e);
          } else {
              // console.log('sound loaded successfully: ' + this.outgoingCall.isLoaded() + " volume is " + this.outgoingCall.getVolume());
             // this.incomingCall.setNumberOfLoops(-1);
              this.outgoingCall.play((success) => {
                  if (success) {
                      // console.log('sound played');
                      this.outgoingCall.release();
                  } else {
                      // console.log('sound failed to play')
                  }
              });
          }
      });
        break;
      case 'incoming':
        this.incomingCall = new Sound('calling.mp3', null, (e) => {
          if (e) {
              // console.log('error', e);
          } else {
            // console.log("***** this.incomingCall********")
            // console.log(this.incomingCall)
              // console.log('sound loaded successfully: ' + this.incomingCall.isLoaded() + " volume is " + this.incomingCall.getVolume());
           //   this.incomingCall.setNumberOfLoops(-1);
              this.incomingCall.play((success) => {
                  if (success) {
                      // console.log('sound played');
                      this.incomingCall.release();
                  } else {
                      // console.log('sound failed to play')
                  }
              });
          }
      });
      break;
      case 'end':
        this.endCall = new Sound('end_call.mp3', null, (e) => {
          if (e) {
              // console.log('error', e);
          } else {
            // console.log("***** this.endCall********")
            // console.log(this.endCall)
            // console.log('sound loaded successfully: ' + this.endCall.isLoaded() + " volume is " + this.endCall.getVolume());
            //  this.endCall.setNumberOfLoops(-1);
              this.endCall.play((success) => {
                  if (success) {
                      // console.log('sound played');
                      this.endCall.release();
                  } else {
                      // console.log('sound failed to play')
                  }
              });
          }
      });
     //   this.endCall.play();
        break;
        default:
        break;
    }
  };



   connexion = () =>{
    this.socket = io(
      // 'https://5d39b882.ngrok.io/webrtcPeer',
      'https://messenger.staging.dsoservices.com/webrtcPeer',
      //'/webrtcPeer',
      {
        path: '/webrtc',
        query: {}
      }
    )


    /***********************  connection-success ****************    */
  this.socket.on('connection-success', success => {
     console.log('success')
    // console.log(success)
  })










  /***********************  offerOrAnswer ****************    */
  this.socket.on('offerOrAnswer', (sp) => {
    return new Promise((resolve, reject) => {
      if(!sp)
      reject()
      else{
      // console.log("************sdp**************")
      // console.log(sp)
      this.sp = JSON.stringify(sp)
      try{
          // console.log("*********** setRemoteDescription **************")
          // console.log(this.pc)
           this.pc.setRemoteDescription(new RTCSessionDescription(sp))
            resolve();
      }catch(e)  {  
        // console.log(this.pc) ; 
        // console.log("*********** catch***************");
      reject();}
      if(!this.state.startCall)
      {
      //  this.playSound('incoming');
        this.setState({startCall:true, anser:true})
        resolve();
      }
    }
       
      });

    })



  /***********************  candidate ****************    */
  this.socket.on('candidate', (candidate) => {
    return new Promise((resolve, reject) => {
      if(!candidate)
      reject()
      else{
      // this.sp = JSON.stringify(sp)
      try{
        this.pc.addIceCandidate(new RTCIceCandidate(candidate))
        resolve();
      }catch(e)  {  
        // console.log(this.pc) ; 
        // console.log("*********** catch***************");
      reject();
    }
  }
    })
  })

 
  this.pc = new RTCPeerConnection({
    "iceServers": [
      // {
      //   urls: 'stun:[STUN_IP]:[PORT]',
      //   'credentials': '[YOR CREDENTIALS]',
      //   'username': '[USERNAME]'
      // },
      {
        urls: 'stun:stun.l.google.com:19302'
      }
    ]
  })



  this.pc.onicecandidate = (e) => {
    return new Promise((resolve, reject) => {
    // send the candidates to the remote peer
    // see addCandidate below to be triggered on the remote peer
    if (e.candidate) {
      //  console.log(JSON.stringify(e.candidate))
      this.sendToPeer('candidate', e.candidate)
      resolve()
    }
    else reject();
  })
  }



  // triggered when there is a change in connection state
  this.pc.oniceconnectionstatechange = (e) => {
    console.log(e)
  }



  this.pc.onaddstream = (e) => {
    return new Promise((resolve, reject) => {
      // debugger
    // this.remoteVideoref.current.srcObject = e.streams[0]
    console.log("*******remoteStream***********") 
    // console.log(e.stream)
    if(e && e.stream)
    {
    this.setState({
      remoteStream: e.stream
    }, console.log("coco"))
    resolve();
  }
  else{
    reject();
  }
  })
  }




  const success = (stream) => {

return new Promise((resolve, reject) => {
if(stream && stream.toURL())
{
    console.log('success stream')
    console.log(stream.toURL())
    this.setState({
      localStream: stream
    })
    console.log(this.state.localStream)
    this.pc.addStream(stream)
    resolve();
  }
  else {
    reject();
  }
  })

  }


  const failure = (e) => {
    console.log('getUserMedia Error: ', e)
  }

  let isFront = true;



  mediaDevices.enumerateDevices().then(sourceInfos => {
    console.log(sourceInfos);
    let videoSourceId;
    for (let i = 0; i < sourceInfos.length; i++) {
      const sourceInfo = sourceInfos[i];
      if (sourceInfo.kind == "videoinput" && sourceInfo.facing == (isFront ? "front" : "environment")) {
        videoSourceId = sourceInfo.deviceId;
      }
    }
    const constraints = {
      audio: true,
      video: {
        mandatory: {
          minWidth: 500, // Provide your own width, height and frame rate here
          minHeight: 300,
          minFrameRate: 30
        },
        facingMode: (isFront ? "user" : "environment"),
        optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
      }
    }
    mediaDevices.getUserMedia(constraints)
      .then(success)
      .catch(failure);
  });

  }





  componentDidMount = () => {
    this.connexion();
  }



  sendToPeer = (messageType, payload) => {
    // return new Promise((resolve, reject) => {
      this.socket.emit(messageType, {
        socketID: this.socket.id,
        payload
      })
    // resolve()
    // })
    }

    createOffer = () => {
      // if(!this.pc)
      // this.connexion();
      console.log('Offer')
      this.setState({ startCall: true }, console.log("coco-offer"))
      // https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/createOffer
      // initiates the creation of SDP
      // return new Promise((resolve, reject) => {

        this.pc.createOffer().then(desc => {
          this.pc.setLocalDescription(desc).then(() => {
            // Send pc.localDescription to peer
            console.log("********** pc.localDescription*******")
            console.log( this.pc.localDescription)
             this.sendToPeer('offerOrAnswer', desc)
          
          });
        });

  }
    
    createAnswer = () => {
      // return new Promise((resolve, reject) => {
      console.log('Answer')
      this.pc.createAnswer({ offerToReceiveVideo: 1 }).then(desc => {

        // this.pc.setLocalDescription(desc)
        // this.sendToPeer('offerOrAnswer', this.pc.localStream)
    
        this.pc.setLocalDescription(desc).then(() => {
          // Send pc.localDescription to peer
          this.sendToPeer('offerOrAnswer', this.pc.localStream)
        });
      });
}


    // setRemoteDescription = () => {
    //   // retrieve and parse the SDP copied from the remote peer
    //   return new Promise((resolve,reject)=>{
    //     if(!this.sp)
    //     reject()
    //     else{
    //   const desc = JSON.parse(this.sp)
  
    //   // set sdp as remote description
    //   this.pc.setRemoteDescription(new RTCSessionDescription(desc))
    //   resolve()
    // }
    // })
    // }

    // addCandidate = () => {
    //   // retrieve and parse the Candidate copied from the remote peer
    //   // const candidate = JSON.parse(this.textref.value)
    //   // console.log('Adding candidate:', candidate)
    //   // add the candidate to the peer connection
    //   // this.pc.addIceCandidate(new RTCIceCandidate(candidate))
  
    //   this.candidates.forEach(candidate => {
    //     console.log(JSON.stringify(candidate))
    //     this.pc.addIceCandidate(new RTCIceCandidate(candidate))
    //   });
    // }
    closeStreams = () => {
  //  this.stopSounds();
    // this.playSound('end');
   

      console.log("**************** will unmount ************")
    if (this.pc) {
      this.pc.removeStream(this.state.localStream)
      this.pc.removeStream(this.state.remoteStream)
      this.pc.close();
    }
  
    this.sp=null;
    this.setState({localStream:null, remoteStream:null})
    this.pc=null,

this.setState({

  localStream: null,
  remoteStream: null,
  sp: "Empty",
  showBouton: true,
  startCall: false,
  toggleMute: false,
  switchCamera: false,
  closeOpenCamera:false,
  anser:false

})

    this.socket = null
    this.candidates = []
    this.outgoingCall = null;
    this.incomingCall = null;
    this.endCall = null;



  //  this.connexion();
    };



    stopSounds = () => {
      if (this.incomingCall && this.incomingCall.isPlaying()) {
       this.incomingCall.pause();   
      }
      if (this.outgoingCall && this.outgoingCall.isPlaying()) {
       this.outgoingCall.pause();
     }
   };



   _moveBall = () => {
    if(this.state.goto===1)
    {
  Animated.spring(this.moveAnimation, {
    toValue: {x: 210, y: 90},
    useNativeDriver: false, // <-- Add this
  }).start()
  this.setState({goto:2})
    }
  else if(this.state.goto===2)
  {
      Animated.spring(this.moveAnimation, {
        toValue: {x: 10, y: 10},
        useNativeDriver: false, // <-- Add this
      }).start()
      this.setState({goto:3})
  }
  else if(this.state.goto===3)
  {
      Animated.spring(this.moveAnimation, {
        toValue: { x: 10, y: 290 },
        useNativeDriver: false, // <-- Add this
      }).start()
      this.setState({goto:1})
  }
}




    //////////////////////////////////

    UICall = () => {
        const {
          localStream,
          remoteStream,
          startCall
        } = this.state


      // if( remoteStream && remoteStream.toURL())
          //  this.stopSounds()
        if (this.state.startCall) {
          return (
            <View>
              <TouchableWithoutFeedback onPress={this.onShow} style={styles.content}>
                <View>
    
                {/* <View style={styles.remoteVideo}> 
                    <RTCView objectFit='cover' style={{ flex: 1, backgroundColor: '#424242', }} streamURL={remoteStream && remoteStream.toURL()} /> 
                </View>  */}

                {/* styles.tennisBall */}
               <Animated.View style={[styles.userVideo, this.moveAnimation.getLayout()]}>
                <TouchableWithoutFeedback style={styles.buttonWebRtc} onPress={this._moveBall}>
                {/* <View style={styles.userVideo}>  */}
                    <RTCView objectFit='cover' zOrder={1} style={{ flex: 1, backgroundColor: '#424242' }} streamURL={localStream && localStream.toURL()}/>
                {/* </View>  */}
                </TouchableWithoutFeedback>  
                </Animated.View>
               
            
          
                  <RTCView
              style={styles.rtcView}
              objectFit={'cover'}
              // objectFit='contain'
              streamURL={remoteStream && remoteStream.toURL()}
               /> 
     
                  {this.paletCamera()}
                </View>
              </TouchableWithoutFeedback>
            </View>
          )
        } else {
          return (
            <View>
              <TouchableOpacity onPress={this.createOffer}>
                <View style={styles.button}>
                  <Text style={{ ...styles.textContent, }}>Call</Text>
                </View>
              </TouchableOpacity>
            </View>
          )
        }
      }

      onShow = () => {
        console.log("clique bouton ")
        this.setState({ showBouton: !this.state.showBouton })
      }

      
  paletCamera = () => {

    if (this.state.showBouton) {
      console.log("this.state.showBouton")
      console.log(this.state.showBouton)
      return (
        <>
          <View style={styles.box11}>
            <View style={styles.box1}>
              {this.cloOpCamera()}
            </View>
          </View>

          {/* <View style={styles.boxText} >
            <Text style={{color:"#fff", fontSize: 20, fontWeight: "bold" }}>ça sonne...</Text>
          </View> */}
         
          {/* <View> <Text> Ca Sonne ...</Text></View> */}
          <View style={styles.box22}>
            <View style={styles.box2}>
              {this.showAnserCall()}
              {this.togMute()}
              {this.switchCam()}
             

              <TouchableWithoutFeedback onPress={this.closeStreams} >
                <View style={styles.close} >
                  <Icon name="times" size={30} color="#fff" />
                </View>
              </TouchableWithoutFeedback>
            </View>
          </View>
        </>
      )
    }
    else {
      return null
    }
  }
  
  toggleMute = () => {
    this.setState({ toggleMute: !this.state.toggleMute })
    if (!this.state.remoteStream) return;
    this.state.localStream.getAudioTracks().forEach(track => {
      // console.log(track.enabled ? 'muting' : 'unmuting', ' local track', track);
      track.enabled = !track.enabled;
      // setIsMuted(!track.enabled);
    });
  };

  switchCamera = () => {
    this.setState({ switchCamera: !this.state.switchCamera })
    this.state.localStream.getVideoTracks().forEach(track => track._switchCamera());
  };

  closeOpenCamera = () => {
    this.setState({ closeOpenCamera: !this.state.closeOpenCamera })
    this.state.localStream.getVideoTracks().forEach(track => track.enabled = !track.enabled);
  }

  cloOpCamera = () =>{
    if (this.state.closeOpenCamera)
    return (
      <TouchableWithoutFeedback onPress={this.closeOpenCamera}>
      <View style={{ ...styles.circle, backgroundColor: 'rgba(255,255, 255, 100)'}} >
        <Icon name="video-camera" size={30} color="#000" />
      </View>
     </TouchableWithoutFeedback> 
    )
  return (
    <TouchableWithoutFeedback onPress={this.closeOpenCamera} >
                <View style={styles.circle} >
                  <Icon name="video-camera" size={30} color="#fff" />
                </View>
    </TouchableWithoutFeedback>
  )
}

  switchCam = () => {
    if (this.state.switchCamera)
      return (
      <TouchableWithoutFeedback onPress={this.switchCamera} > 
          <View style={{ ...styles.circle, backgroundColor: 'rgba(255,255, 255, 100)' }} >
            <Icon name="camera" size={30} color="#000" />
          </View>
      </TouchableWithoutFeedback>
      )
    return (
      <TouchableWithoutFeedback onPress={this.switchCamera} >
        <View style={styles.circle} >
          <Icon name="camera" size={30} color="#fff" />
        </View>
      </TouchableWithoutFeedback>
    )
  }

  togMute=()=>{
    if (this.state.toggleMute)
    return (
  <TouchableWithoutFeedback onPress={this.toggleMute} >
                <View style={{ ...styles.circle, backgroundColor: 'rgba(255,255, 255, 100)' }} >
                  <Icon name="microphone" size={30} color="#000" />
                </View>
  </TouchableWithoutFeedback>
    )
  return (
    <TouchableWithoutFeedback onPress={this.toggleMute} >
                <View style={styles.circle} >
                  <Icon name="microphone" size={30} color="#fff" />
                </View>
    </TouchableWithoutFeedback>
  )
  }


  showAnserCall=()=>{
    if(this.state.anser)
    return (
  <TouchableWithoutFeedback onPress={this.createAnswer} >
                <View style={{ ...styles.circle, backgroundColor: 'rgba(255,255, 255, 100)' }} >
                  <Icon name="phone" size={30} color="#000" />
                </View>
  </TouchableWithoutFeedback>
    )
    else 
    return null;
  }

  // closeStreams = () => {
  //   //this.playSound('end');
  //  // this.stopSounds();

  //   if (this.pc) {
  //     this.pc.removeStream(this.state.localStream)
  //     this.pc.removeStream(this.state.remoteStream)
  //     this.pc.close();
  //   }
  //   this.setState({ localStream: null, remoteStream: null })
  //   this.pc = null
  //   this.setState({ startCall: false })



  //   await this.connexion();
  // };
    


  render() {
    const {
      localStream,
      remoteStream,
    } = this.state

    if(!this.pc)
    this.connexion();


    return (
      <View>
        {this.UICall()}
      </View>

      );
  }
};

const styles = StyleSheet.create({
  buttonsContainer: {
    flexDirection: 'row',
  },
  content: {
    flex: 1,

    alignContent: 'center'
  },
  box22: {

    justifyContent: 'center',
    alignItems: 'center',
  },
  box1: {
    position: 'absolute',
    top: 20,
    // marginRight:20,
    // justifyContent:'flex-end',
    alignItems: 'flex-end'

  },
  box11: {
    // marginRight:20
    position: 'absolute',
    alignItems: 'flex-end',
    alignSelf: 'flex-end'

  },
  buttonVisible: {

    // button:0
    // left: 40,
    // width: 100,
    // height: 100,
    // backgroundColor: 'red'
  },
  box2: {
    // flex: 1,
    position: 'absolute',
    bottom: 50,
    flexDirection: 'row',
    // justifyContent:'center',
    // alignItems:'center',
    // alignContent:'center'

  },
  boxText:{
    marginTop:100,
    position: 'absolute',
    alignItems: 'center',
    alignSelf: 'center'
  },
  buttonTop: {
    // flex: 1,
    //    position: 'absolute',
    //   bottom: 50, 
    flexDirection: 'row',
    // justifyContent:'center',
    // alignItems:'center',
    // alignContent:'center'
  },

  button: {
    margin: 5,
    paddingVertical: 10,
    backgroundColor: 'lightgrey',
    borderRadius: 5,
  },
  textContent: {
    fontFamily: 'Avenir',
    fontSize: 20,
    textAlign: 'center',
  },
  videosContainer: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',

  },
  rtcView: {
    //  width: 300, //dimensions.width,
    //  height: 200,//dimensions.height / 2,

    // backgroundColor: 'black',
    // windowWidth: Dimensions.get('window').width,
    // windowHeight: Dimensions.get('window').height
    // flex: 1,
    // ...StyleSheet.absoluteFillObject,
    // position: 'absolute',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,

  },
  scrollView: {
    flex: 1,
    // flexDirection: 'row',
    backgroundColor: 'teal',
    padding: 15,
  },
  rtcViewRemote: {
    // // position: 'absolute',
    // width: dimensions.width - 30,
    // height: 200,//dimensions.height / 2,
     backgroundColor: 'black',
    width: Dimensions.get('window').width,
    height: Dimensions.get('window').height,

  },

  circle: {
    alignItems: "center",
    justifyContent: "center",
    width: 50,
    height: 50,
    borderRadius: 100 / 2,
    backgroundColor: 'rgba(255,255, 255, 0.2)',
    margin: 10
  },
  close: {
    alignItems: "center",
    justifyContent: "center",
    width: 50,
    height: 50,
    borderRadius: 100 / 2,
    backgroundColor: 'rgba(255,0, 0, 100)',
    margin: 10
  },
  rtc: {
    width: '80%',
    height: '100%',
  },
  container:{
    flex:1,
    backgroundColor: "black"
},
userVideo:{
    backgroundColor: 'black',
    position:"absolute",
    // borderRadius:80,
    left: "55%",
    top: "50%",
    width: "40%",
    height: "28%",
    overflow:'hidden',
    zIndex:2
},
remoteVideo:{
    backgroundColor: 'black',
    borderColor: 'black',
    flex:1,
    // borderWidth: 5,s
    // borderRadius: 20,
},
tennisBall: {
  display: 'flex',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'greenyellow',
  borderRadius: 100,
  width: 100,
  height: 100,
},
buttonWebRtc: {
  paddingTop: 24,
  paddingBottom: 24,
}
});


export default App;
